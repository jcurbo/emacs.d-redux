(require 'cl)

; package.el
(require 'package)
(setq package-archives '(
             ("melpa" . "http://melpa.org/packages/")
			 ("gnu" . "http://elpa.gnu.org/packages/")
			 ))
(package-initialize)

; cask and pallet

; cask
(cond ((equal system-type 'gnu/linux)
        (progn (require 'cask "/home/james/.cask/cask.el")))
      ((equal system-type 'windows-nt)
        (progn (require 'cask "c:\\Users\\james\\Documents\\code\\cask\\cask.el"))))

(cask-initialize)
(require 'pallet)
(pallet-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; highlights matching parens
(setq show-paren-mode t)

; I hate backups
(setq backup-inhibited t)
(setq auto-save-default nil)

; cut/copy/paste
(cua-mode t)

; don't need the startup screen
(setq inhibit-startup-screen t)

; recent files mode
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 25)

(require 'rainbow-delimiters)

(setq ispell-program-name "aspell")
(setq ispell-dictionary "english")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; fonts and colors
(global-font-lock-mode 1)
(setq font-lock-maximum-decoration t)
 (load-theme 'solarized-dark t)

(cond ((or (equal system-type 'darwin) (equal system-type 'gnu/linux))
		(progn
		  (set-face-attribute 'default nil
			:family "Inconsolata" :height (case system-type
							('gnu/linux 120)
							('darwin 180)) :weight 'normal))    )
	  ((equal system-type 'windows-nt)
		(set-face-attribute 'default nil
				    :family "Consolas" :height 120)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; tabbar
(require 'tabbar)
(require 'tabbar-ruler)

(setq tabbar-seperator (quote (0.5)))

(setq tabbar-buffer-groups-function
          (lambda ()
            (list "All")))

(setq tabbar-buffer-list-function
       (lambda ()
         (remove-if
          (lambda(buffer)
            (find (aref (buffer-name buffer) 0) " *"))
          (buffer-list))))

(defun tabbar-buffer-tab-label (tab)
  "Return a label for TAB.
That is, a string used to represent it on the tab bar."
  (let ((label (if tabbar--buffer-show-groups
		   (format "[%s] " (tabbar-tab-tabset tab))
		 (format "%s " (tabbar-tab-value tab)))))
    ;; Unless the tab bar auto scrolls to keep the selected tab
    ;; visible, shorten the tab label to keep as many tabs as possible
    ;; in the visible area of the tab bar.
    (if tabbar-auto-scroll-flag
	label
      (tabbar-shorten
       label (max 1 (/ (window-width)
		       (length (tabbar-view
				(tabbar-current-tabset)))))))))

(tabbar-mode t)

(setq tabbar-ruler-global-tabbar t) ; If you want tabbar

(set-face-attribute 'tabbar-default nil :family "Inconsolata" :height 120)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; setup path
(setenv "PATH" (shell-command-to-string "source ~/.profile; echo -n $PATH"))
;; Update exec-path with the contents of $PATH
(loop for path in (split-string (getenv "PATH") ":") do 
      (add-to-list 'exec-path path))

;; Grab other environment variables
(loop for var in (split-string (shell-command-to-string "source ~/.profile; 
env")) do
      (let* ((pair (split-string var "="))
             (key (car pair))
             (value (cadr pair)))
        (unless (getenv key)
          (setenv key value))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; flycheck
(global-flycheck-mode)
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))

(require 'flycheck-color-mode-line)

(eval-after-load "flycheck"
                   '(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; company
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; haskell
(require 'haskell-mode)
(require 'haskell-interactive-mode)
(require 'haskell-process)
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)

; this gets rid of flymake
(delete '("\\.l?hs\\'" haskell-flymake-init) flymake-allowed-file-name-masks)

(autoload 'ghc-init "ghc" nil t)
(autoload 'ghc-debug "ghc" nil t)
(add-hook 'haskell-mode-hook (lambda () (ghc-init)))

(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

;(eval-after-load 'haskell-mode '(progn
  (define-key haskell-mode-map [f8] 'haskell-navigate-imports)
  (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
  (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
  (define-key haskell-mode-map (kbd "C-c C-n C-t") 'haskell-process-do-type)
  (define-key haskell-mode-map (kbd "C-c C-n C-i") 'haskell-process-do-info)
  (define-key haskell-mode-map (kbd "SPC") 'haskell-mode-contextual-space)
  (define-key haskell-mode-map (kbd "C-c C-o") 'haskell-compile)
;))

(add-to-list 'company-backends 'company-ghc)
(add-hook 'haskell-mode-hook #'rainbow-delimiters-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; my menu stuff
(require 'easymenu)
(easy-menu-define my-menu global-map "MyMenu"
  '("MyMenu"
    ["Open Config" (find-file user-init-file)]
    ["Run Racket REPL" (run-racket)]
    ))
(easy-menu-remove-item global-map '("menu-bar") "MyMenu")
(easy-menu-add-item nil nil my-menu)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; evil-nerd-commenter
(global-set-key "\M-;" 'evilnc-comment-or-uncomment-lines)
(global-set-key "\M-:" 'evilnc-comment-or-uncomment-to-the-line)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; markdown
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;(add-hook 'markdown-mode-hook 'turn-on-pandoc)
(add-hook 'markdown-mode-hook 'pandoc-mode)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; web stuff
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(autoload 'css-mode "css-mode" "Mode for editing CSS files" t)
(add-to-list 'auto-mode-alist '("\\.css$" . css-mode))

(autoload 'js2-mode "js2" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; org-mode
(require 'org)
(setq org-startup-folded nil)
(setq org-startup-indented t)

(setq org-directory "~/Dropbox/org")
(setq org-mobile-inbox-for-pull "~/Dropbox/org/flagged.org")
(setq org-mobile-directory "~/Dropbox/Apps/MobileOrg")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; customize mode stuff
; keep this at the end
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-ghc-show-info t)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(flymake-allowed-file-name-masks nil)
 '(haskell-indent-spaces 4)
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-process-type (quote stack-ghci))
 '(haskell-tags-on-save t)
 '(show-paren-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; smart mode line
; needs to be after custom-set-variables for custom-safe-themes to work 
(setq sml/theme 'dark)
(require 'smart-mode-line)
(sml/setup)
